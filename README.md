# Getting started

This is a [Django](http://www.djangoproject.com) project to be deployed it on [Vercel](https://vercel.com/). To make it easy for you to get started, here's a list of recommended next steps. 

The [next step](#virtual-enviroment-and-libraries) in this document assume that you already have  <strong>Python</strong> installed!

# Virtual enviroment and libraries

This is a minimal Django 4.2 project. If you want to deploy go to [deploying](#deploying-to-vercel) step. To run this project localy, follow the next steps:

**IMPORTANT:** To create a Telegram Bot and get it's token go to [create a new bot](https://core.telegram.org/bots/features#creating-a-new-bot).

<ol>
    <li>Create and activate a virtualenv</li>
    <li>
        Manually install the dependencies with th command: <span style="color: orange">pip install -r requirements.txt</span>
    </li>
    <li>
        Configure a .env with folowing setting:
<div>

    # Django
    DJANGO_SECRET_KEY=your_secret_key
    DJANGO_ADMINS=Admin <admin@admin.com>
    DJANGO_LOG_LEVEL=DEBUG

    # Email
    DEFAULT_FROM_EMAIL=Default <your.default@email.com>
    EMAIL_PORT=587
    EMAIL_HOST_USER=your.default@email.com
    EMAIL_HOST_PASSWORD=email_password

    # Telegram
    TELEGRAM_BOT_TOKEN=your_telegram_bot_token
    TELEGRAM_BOT_SECRET=webhook_secret
</div>
    </li>
    <li>
        Create a development database: <span style="color: orange">./manage.py migrate</span>
    </li>
    <li>
        If everything is alright, you should be able to start the Django development server: <span style="color: orange">./manage.py runserver</span>
    </li>
</ol>


**NOTE:** To configure the telegram webhook to run locally, go to the [next step](#configuring-the-webhook).

## Configuring the webhook
To run the server locally, use [Ngrok](https://ngrok.com/) CLI tool run the command <span style="color: orange">ngrok http 8000</span> to create a valid url for that Telegram API Bot can send the requests. Exmaple: https://a123-456-78-90.ngrok-free.app

Set this into [Django “sites” framework](https://docs.djangoproject.com/en/4.2/ref/contrib/sites/) inside Django admin page. Go to to http://127.0.0.1:8000/telegram/settings (only for superusers)and click "Set webhook"  button.

## Deploying to Vercel
Set the following enviroment variables into the vercel project. 

**NOTE:** In this case, we are using PostgreSQL.

```
# Django
DJANGO_ALLOWED_HOSTS=.vercel.app
DJANGO_SETTINGS_MODULE=website.settings.vercel
DJANGO_SECRET_KEY=your_secret_key
DJANGO_ADMINS=Admin <admin@admin.com>
DJANGO_LOG_LEVEL=INFO


# Email
DEFAULT_FROM_EMAIL=Default <your.default@email.com>
EMAIL_PORT=587
EMAIL_HOST_USER=your.default@email.com
EMAIL_HOST_PASSWORD=email_password

# Telegram
TELEGRAM_BOT_TOKEN=your_telegram_bot_token
TELEGRAM_BOT_SECRET=webhook_secret

# Database
DATABASE_ENGINE=django.db.backends.postgresql
DATABASE_NAME=db_name
DATABASE_USER=db_user
DATABASE_PASSWORD=db_password
DATABASE_HOST=db_host
DATABASE_PORT=db_port
```
