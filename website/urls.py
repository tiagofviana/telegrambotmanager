from django.conf import settings
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include

urlpatterns = [
    # Admin
    path('admin/login/', lambda request: redirect(settings.LOGIN_URL)),
    path('admin/', admin.site.urls),

    # Default static files
    path('favicon.ico', lambda request: redirect('/static/core/favicons/favicon.ico')),

    # Apps
    path('', include('users.urls')),
    path('telegram/', include('telegram.urls')),
]

