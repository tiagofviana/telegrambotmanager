import logging


class LogErroMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    @staticmethod
    def _log_erro(request, name: str):
        method = request.META.get('REQUEST_METHOD')
        protocol = request.META.get('SERVER_PROTOCOL')
        xForwardedFor = request.META.get('HTTP_X_FORWARDED_FOR')
        remoteAddress = request.META.get('REMOTE_ADDR')
        logging.warning(
            f"{name} at {method} {request.path} {protocol}."
            f" [User agent: {request.META.get('HTTP_USER_AGENT')}]"
            f" [X_FORWARDED_FOR: {xForwardedFor} – REMOTE_ADDR: {remoteAddress}]"
        )

    def __call__(self, request):
        response = self.get_response(request)

        if response.status_code == 404:
            self._log_erro(request, name='Page not found (404)')

        if response.status_code == 403:
            self._log_erro(request, name='Permission denied (403)')

        return response
