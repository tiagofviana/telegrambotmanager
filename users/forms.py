from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.translation import gettext_lazy as _
from .models import CustomGroup


class CustomGroupAdminForm(forms.ModelForm):
    class Meta:
        model = CustomGroup
        fields = '__all__'
        widgets = {
            'permissions': FilteredSelectMultiple(_('Permissions'), is_stacked=False)
        }


class CustomAuthenticationForm(AuthenticationForm):
    error_messages = {
        'invalid_login': _("Email and password incorrect, try again"),
        'invalid_permission': _("This user does not have access permission"),
        'inactive': _("This account has been deactivated"),
    }

    def __init__(self, request, *args, **kwargs):
        super().__init__(request, *args, **kwargs)
        
        self.fields["username"].widget.attrs.pop("autofocus", None)
        
        for field_name in self.fields:
            self.fields[field_name].widget.attrs["placeholder"] = " "
            self.fields[field_name].widget.attrs["class"] = "form-control"

            if self.has_error(field=field_name):
                self.fields[field_name].widget.attrs["class"] += " is-invalid"

    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        if username is not None and password:
            self.user_cache = authenticate(
                self.request, username=username, password=password
            )
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            
            if self.user_cache.is_staff is False:
                raise forms.ValidationError(self.error_messages['invalid_permission'])
            
            self.confirm_login_allowed(self.user_cache)