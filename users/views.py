
import logging
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.views import LoginView
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from .forms import CustomAuthenticationForm

class Login(LoginView):
    form_class=CustomAuthenticationForm
    template_name="users/login.html"

    def get_success_url(self):
        return reverse('admin:index')
    
    def form_invalid(self, form: AuthenticationForm) -> HttpResponse:
        msg = "Invalid login username: \"%s\", password: \"%s\"" % (
            form.cleaned_data.get('username', ""),
            form.cleaned_data.get('password', ""),
        )
        logging.info(msg)
        return super().form_invalid(form)
