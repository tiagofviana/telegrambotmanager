from django.contrib import admin
from django.contrib.auth import get_user_model, admin as auth_admin
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _

from .models import CustomGroup
from .forms import CustomGroupAdminForm


admin.site.unregister(Group)


@admin.register(CustomGroup)
class CustomGroupAdmin(admin.ModelAdmin):
    form = CustomGroupAdminForm


@admin.register(get_user_model())
class UserAdmin(auth_admin.UserAdmin):
    model = get_user_model()
    list_display = ('full_name', 'email')
    ordering = ('email', )
    list_filter = ()
    search_fields = ()
    readonly_fields = ('registration_date', 'is_superuser', 'last_login', 'id')
    add_fieldsets = (
        (_('Identification'), {
            'fields': ('first_name', 'last_name'),
        }),
        
        (_('Login data'), {
                'fields': ('email', 'password1', 'password2')
        }),

        (_('Permission'), {
            'fields': ('is_superuser', 'is_active', 'is_staff', 'groups', 'user_permissions')
        }),
    )
    fieldsets = (        
        (_('Identification'), {
            'fields': ('id', ('first_name', 'last_name'), ),
        }),

        (_('Login data'), {
            'fields': ('email', 'password', 'last_login')
        }),

        (_('System data'), {
            'fields': ('registration_date',),
        }),


        (_('Permission'), {
            'fields': ('is_superuser', 'is_staff', 'is_active', 'groups', 'user_permissions')
        }),
    )

