from django.conf import settings
from django.urls import path
from django.shortcuts import redirect
from django.contrib.auth.views import LogoutView 

from . import views

app_name = 'users'

urlpatterns = [
    path('', lambda request: redirect('users:login')),
    path('login', views.Login.as_view(), name='login'),
    path('logout', LogoutView.as_view(next_page=settings.LOGIN_URL), name='logout'),
]
