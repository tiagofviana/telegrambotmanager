from django.db import models
from django.core.mail import send_mail as django_send_mail
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, Group
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password, **other_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **other_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **other_fields):
        other_fields.setdefault('is_superuser', True)
        other_fields.setdefault('is_staff', True)
        return self.create_user(email, password, **other_fields)



class User(AbstractBaseUser, PermissionsMixin):
    # Os campos password, last_login, is_superuser são criados automaticamente do AbstractBaseUser
    id = models.AutoField('ID', unique=True, auto_created=True, primary_key=True, db_index=True)
    email = models.EmailField(unique=True, max_length=100, blank=False, null=False, db_index=True)
    first_name = models.CharField(_('first name'), max_length=50, blank=False, null=False)
    last_name = models.CharField(_('last name'), max_length=100, blank=False, null=False)
    registration_date = models.DateTimeField(_('registration date'), default=timezone.now)
    is_staff = models.BooleanField(_('is a staff'), default=False)
    is_active = models.BooleanField(_('is active'), default=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        managed = True
        db_table = 'user'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def full_name(self):
        """ Return the 'first_name' and the 'lastName' with a space in between """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()
    full_name.short_description = _('full name')
    
    def __str__(self):
        return self.email

    def send_email(self, subject, html_message, fail_silently=True):
        django_send_mail(
            subject=subject,
            html_message=html_message,
            recipient_list=[self.email],
            message=None,
            from_email=None,
            fail_silently=fail_silently,
        )

class CustomGroup(Group):
    """Created only to group in the admin"""

    class Meta:
        proxy = True
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')
