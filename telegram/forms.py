from abc import abstractmethod
import inspect
import logging
from django import forms
from django.utils.translation import gettext_lazy as _

from telegram.commands import CommandManager

from .models import Account, ChatMessage

ERRO_MESSAGES = {
    'bot': _("We don't process commands from bots"),
    'blocked': _("Your account was blocked, talk to the support team to find out more"),
    'invalid_command': _("This command is not valid"),
}

class FormNotImplementedError(NotImplementedError):
    def __init__(self, message = None, *args, **kwargs):
        if message is None:
            stack = inspect.stack()
            function_name = stack[1].function
            class_name = stack[1][0].f_locals['self'].__class__.__name__
            message = f'The \"{function_name}\" method of \"{class_name}\" is not implemented'
        return super().__init__(message, *args, **kwargs)


"""PRIVATE CHAT FORMS """

class PrivateChatForm(forms.Form):
    """This form is an absctract form for private chat forms"""
    json_data = forms.JSONField()
  
    def clean_json_data(self):
        json_data = self.cleaned_data.get('json_data')
        if 'update_id' not in json_data:
            raise forms.ValidationError(_("There isn't an update_id"))
        return json_data
    
    def validate_telegram_user_data(self):
        from_data = self.user_data
        keys = ['id', 'is_bot', 'first_name']

        for key in keys:
            if key not in from_data:
                raise forms.ValidationError(_(f"Missing \"{key}\" data"))
            
    @property
    def user_data(self) -> dict:
        raise FormNotImplementedError()
    
    @property
    def message_text(self)-> str:
        raise FormNotImplementedError()

    @property
    def message_id(self)-> int:
        raise FormNotImplementedError()
    
    @property
    def update_id(self) -> int:
       return self.cleaned_data['json_data']['update_id']    
    
    @property
    def telegram_id(self) -> int:
        return self.user_data['id']
    
    @property
    def is_bot(self) -> bool:
        return self.user_data['is_bot']
    
    @property
    def first_name(self):
        return self.user_data['first_name']

    @property
    def account(self) -> Account:
        if hasattr(self, '_account'):
            return self._account
        self._account = Account.get_or_create_and_update(
            telegram_id=self.telegram_id,
            is_bot=self.is_bot,
            first_name=self.user_data['first_name'],
            # Optional
            last_name=self.user_data.get('last_name', None),
            username=self.user_data.get('username', None)
        )
        return self._account

    
    def get_body_reponse(self) -> dict:
        if ChatMessage.is_already_responded(self.update_id):
            logging.debug("Already responded")
            return {}
        
        if self.account.is_bot:
            logging.debug("Bot detected")
            return self._bot_response

        if self.account.is_blocked:
           logging.debug("Account blocked")
           return self._blocked_response
        
        manager = CommandManager(
            message=self.message_text, 
            message_id=self.message_id, 
            account=self.account,
        )
        if manager.command_exists():
            return manager.execute()
        
        return self._invalid_command()

        
    def _invalid_command(self):
        return {
            'method': 'sendMessage',
            'chat_id': self.telegram_id,
            'text': ERRO_MESSAGES['invalid_command'].__str__(),
            'reply_to_message_id': self.message_id,
        }
    
    def _bot_response(self):
        return {
            'method': 'sendMessage',
            'chat_id': self.telegram_id,
            'text': str(ERRO_MESSAGES['bot']),
            'reply_to_message_id': self.message_id,
        }
    
    def _blocked_response(self):
        return {
            'method': 'sendMessage',
            'chat_id': self.telegram_id,
            'text': str(ERRO_MESSAGES['blocked_account']),
            'reply_to_message_id': self.message_id,
        }


class PrivateChatMessageForm(PrivateChatForm):
    def clean_json_data(self):
        json_data = super().clean_json_data()

        if 'message' not in json_data:
            raise forms.ValidationError(_("Missing \"message\" data"))
        
        if 'message_id' not in json_data['message']:
            raise forms.ValidationError(_("Missing \"message_id\" data"))
        
        if 'text' not in json_data['message']:
            raise forms.ValidationError(_("Missing \"text\" data"))
        
        if 'from' not in json_data['message']:
            raise forms.ValidationError(_("Missing \"from\" data"))

        self.validate_telegram_user_data()
        
        return json_data

    @property
    def user_data(self) -> dict:
        from_data = self.cleaned_data.get('json_data', {})\
            .get('message', {})\
                .get('from', {})
        return from_data
    
    @property
    def message_text(self)-> str:
        return self.cleaned_data['json_data']['message']['text']

    @property
    def message_id(self)-> int:
        return self.cleaned_data['json_data']['message']['message_id']


class PrivateChatEditedMessageForm(PrivateChatForm):
    def clean_json_data(self):
        json_data = super().clean_json_data()

        if 'edited_message' not in json_data:
            raise forms.ValidationError(_("Missing \"edited_message\" data"))
        
        if 'message_id' not in json_data['edited_message']:
            raise forms.ValidationError(_("Missing \"message_id\" data"))
        
        if 'text' not in json_data['edited_message']:
            raise forms.ValidationError(_("Missing \"text\" data"))
        
        if 'from' not in json_data['edited_message']:
            raise forms.ValidationError(_("Missing \"from\" data"))

        self.validate_telegram_user_data()
        
        return json_data

    @property
    def user_data(self) -> dict:
        from_data = self.cleaned_data.get('json_data', {})\
            .get('edited_message', {})\
                .get('from', {})
        return from_data
    
    @property
    def message_text(self)-> str:
        return self.cleaned_data['json_data']['edited_message']['text']

    @property
    def message_id(self)-> int:
        return self.cleaned_data['json_data']['edited_message']['message_id']


class PrivateChatCallbackQueryForm(PrivateChatForm):
    def clean_json_data(self):
        json_data = super().clean_json_data()
        
        if 'callback_query' not in json_data:
            raise forms.ValidationError(_("There isn't a \"callback_query\" data"))
        
        if 'from' not in json_data['callback_query']:
            raise forms.ValidationError(_("Missing \"from\" data"))
        
        if 'message' not in json_data['callback_query']:
            raise forms.ValidationError(_("Missing \"message\" data"))
        
        if 'message_id' not in json_data['callback_query']['message']:
            raise forms.ValidationError(_("Missing \"message_id\" data"))
        
        if 'text' not in json_data['callback_query']['message']:
            raise forms.ValidationError(_("Missing \"text\" data"))
        
        self.validate_telegram_user_data()

        return json_data

    @property
    def user_data(self) -> dict:
        from_data = self.cleaned_data.get('json_data', {})\
            .get('callback_query', {})\
                .get('from', {})
        return from_data
    
    @property
    def message_text(self)-> str:
        return self.cleaned_data['json_data']['callback_query']['data']

    @property
    def message_id(self)-> int:
        return self.cleaned_data['json_data']['callback_query']['message']['message_id']
    

""" GROUP FORMS """

class GroupForm(forms.Form):
    """This form is an absctract form for groups forms"""
    json_data = forms.JSONField()

    def get_body_reponse(self) -> dict:
        return {
            'method': 'leaveChat',
            'chat_id': self.chat_id,
        }

    @property
    def chat_id(self) -> int:
        raise FormNotImplementedError()
    
    @abstractmethod
    def clean_json_data(self):
        raise FormNotImplementedError()


class GroupMyChatMemberForm(GroupForm):
    def clean_json_data(self):
        json_data = self.cleaned_data['json_data']
        if 'my_chat_member' not in json_data:
            raise forms.ValidationError(_("Missing \"my_chat_member\" data"))
        
        if 'chat' not in json_data['my_chat_member']:
            raise forms.ValidationError(_("Missing \"chat\" data"))
        
        if 'id' not in json_data['my_chat_member']['chat']:
            raise forms.ValidationError(_("Missing \"id\" data"))
        
        return json_data

    @property
    def chat_id(self) -> int:
        json_data = self.cleaned_data['json_data']
        return json_data['my_chat_member']['chat']['id']


class GroupNewChatMemberForm(GroupForm):
    def clean_json_data(self):
        json_data = self.cleaned_data['json_data']
        if 'message' not in json_data:
            raise forms.ValidationError(_("Missing \"message\" data"))
        
        if 'chat' not in json_data['message']:
            raise forms.ValidationError(_("Missing \"chat\" data"))
        
        if 'id' not in json_data['message']['chat']:
            raise forms.ValidationError(_("Missing \"id\" data"))
        
        return json_data

    @property
    def chat_id(self) -> int:
        json_data = self.cleaned_data['json_data']
        return json_data['message']['chat']['id']
    


    