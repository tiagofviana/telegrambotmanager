import json
import logging
from django.conf import settings
from django.http import Http404, JsonResponse
from django.shortcuts import redirect, render
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import SuspiciousOperation
from django.contrib import messages

import requests
from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import HtmlFormatter

from .models import ChatMessage
from .forms import PrivateChatForm, PrivateChatMessageForm, PrivateChatEditedMessageForm, PrivateChatCallbackQueryForm, GroupMyChatMemberForm, GroupForm, GroupNewChatMemberForm



class SettingsView(View):
    http_method_names = ['get', 'post', 'delete']

    def has_permission(self):
        if not self.request.user.is_superuser:
            return False
        
        return True

    def dispatch(self, request, *args, **kwargs):
        if not self.has_permission():
            raise Http404()
        
        return super().dispatch(request, *args, **kwargs)


    def get(self, request):

        result = requests.get(
            '{telegram}{token}/getWebhookInfo'.format(
                telegram = settings.TELEGRAM_BOT_API_URL,
                token = settings.TELEGRAM_BOT_TOKEN
            )        
        ).json()

        result_json = json.dumps(result, sort_keys=True, indent=4, ensure_ascii=False)
        formatter = HtmlFormatter(style='rainbow_dash')
        result_json_formated = highlight(result_json, JsonLexer(), formatter)

        context = {
            'result_json_formated': result_json_formated,
            'result_url': result.get('result', {}).get('url', ""),
            'formatter_style': formatter.get_style_defs(),
        }

        return render(request, 'telegram/admin/settings.html', context)
    
    def delete_webhook(self):
        requests.get(
            '{telegram}{token}/deleteWebhook'.format(
                telegram = settings.TELEGRAM_BOT_API_URL,
                token = settings.TELEGRAM_BOT_TOKEN
            )        
        )
        messages.success(self.request, _("Webhook was deleted"))
    
    def set_webhook(self):
        url = "https://" + str(get_current_site(self.request)) + reverse('telegram:webhook')
        print(url)
        url = '{telegram}{token}/setWebhook?url={url}&secret_token={secret_token}'.format(
                telegram = settings.TELEGRAM_BOT_API_URL,
                token = settings.TELEGRAM_BOT_TOKEN,
                url = url,
                secret_token = settings.TELEGRAM_BOT_SECRET
            ) 
        print(url)
        requests.get(
            url    
        )
        messages.success(self.request, _("Webhook was set"))

    def post(self, request):

        if '_delete_webhook' in request.POST:
            self.delete_webhook()

        if '_set_webhook' in request.POST:
            self.set_webhook()
        
        return redirect('admin:index')


class Webhook(View):
    http_method_names = ['post',]

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request) -> JsonResponse:
        secret_token = request.headers.get('X-Telegram-Bot-Api-Secret-Token', None)
        if secret_token != settings.TELEGRAM_BOT_SECRET:
            raise Http404()
        
        private_chat_form = self._private_chat_form({'json_data': request.body})
        if private_chat_form is not None:
            response_body = private_chat_form.get_body_reponse()

            ChatMessage.objects.create(
                Account_id = private_chat_form.account, 
                update_id =private_chat_form.update_id,
                message_id = private_chat_form.message_id,
                text = private_chat_form.message_text,
                request_body = private_chat_form.cleaned_data['json_data'],
                response_body = response_body
            )
            return JsonResponse(response_body)

        group_form = self._group_form({'json_data': request.body})
        if group_form is not None:
            response_body = group_form.get_body_reponse()
            return JsonResponse(response_body)

        raise SuspiciousOperation(f"Invalid request body: {request.body}.")


    def _private_chat_form(self, form_data: dict) -> PrivateChatForm or None:
        for update_form in [PrivateChatMessageForm, PrivateChatEditedMessageForm, PrivateChatCallbackQueryForm]:
            form = update_form(data=form_data)

            if not issubclass(form.__class__, PrivateChatForm):
                raise Exception("Form is nos a sub class of PrivateChatForm")

            if form.is_valid():
                return form
    
    def _group_form(self, form_data: dict) -> GroupForm or None:
        for chat_form in [GroupNewChatMemberForm, GroupMyChatMemberForm]:
            form = chat_form(data=form_data)

            if not issubclass(form.__class__, GroupForm):
                raise Exception("Form is nos a sub class of GroupForm")

            if form.is_valid():
                return form
