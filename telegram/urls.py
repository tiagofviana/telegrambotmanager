from django.urls import path

from . import views

app_name = 'telegram'

urlpatterns = [
    path('settings', view=views.SettingsView.as_view(), name='settings'),
    path('webhook', view=views.Webhook.as_view(), name='webhook'),
]
