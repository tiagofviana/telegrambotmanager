from .base import BaseCommand


class Command(BaseCommand):    
    def process(self) -> dict:
        message = self.render_template('telegram/commands/suporte.html')
        return  {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': message,
            'reply_to_message_id': self.message_id,
        }