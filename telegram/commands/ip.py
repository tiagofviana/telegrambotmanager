import re
import logging
import requests
from django.utils.translation import gettext as _
from .base import BaseCommand


class Command(BaseCommand):

    def process(self) -> dict:

        if self.argument == "ajuda":
            return self._help_body_reponse()

        if not self.is_valid():
            return self._not_valid_response()
        
        return self.search()
    
    def search(self) -> str:
        context = {}
        context['ip'] = self.argument
        context['search'] = requests.get(f"http://ip-api.com/json/{self.argument}").json()
        message = self.render_template('telegram/commands/ip/search.html', context)
        logging.debug(f"IP search message: {message}")
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': message,
            'reply_to_message_id': self.message_id,
        }
          
    def is_valid(self):
        pattern=r"^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$"
        match_obj = re.fullmatch(pattern=pattern, string=self.argument)

        if match_obj is None:
            return False
            
        return True
    
    def _not_valid_response(self) -> dict:
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': _("This is not a valid IP. For more details press the button below:"),
            'reply_to_message_id': self.message_id,
            'reply_markup': {
                'inline_keyboard': [
                    [{'text': "IP", 'callback_data': '/ip ajuda'},]
                ]
            }
        }
    
    def _help_body_reponse(self) -> dict:
        message = self.render_template('telegram/commands/ip/help.html')
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': message,
            'reply_to_message_id': self.message_id,
        }