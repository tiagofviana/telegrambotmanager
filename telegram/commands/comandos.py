from .base import BaseCommand
from django.utils.translation import gettext as _

class Command(BaseCommand):
    def process(self) -> dict:
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': _("COMMAND LIST:"),
            'reply_to_message_id': self.message_id,
            'reply_markup': {
                'inline_keyboard': [
                    [{'text': "CNPJ", 'callback_data': '/cnpj ajuda'},],
                    [{'text': "IP", 'callback_data': '/ip ajuda'},]
                ]
            }
        }
   