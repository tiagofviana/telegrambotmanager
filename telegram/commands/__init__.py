import os
import imp
import logging
from pathlib import Path

from ..models import Account
from .base import BaseCommand

_COMMANDS_DIR = Path(__file__).parent.resolve()


class CommandManager:
    def __init__(self, message: str, message_id: int, account: Account):
        items = message.split(' ', 1)
        self.command = items[0]
        self.argument = items[1] if len(items) > 1 else ""
        self.account = account
        self.message_id = message_id

    def __str__(self) -> str:
        return f'Command Manager: "{self.command}". Data: "{self.argument}"'
    
    def execute(self) -> dict:
        logging.debug(f"Starting to execute the command: {self.command}")
        command = self._get_command()
        return command.process()

    @staticmethod
    def _get_all_commands() -> list:
        return [
            f"/{name[:-3]}" for name in os.listdir(_COMMANDS_DIR)
                if os.path.isfile(_COMMANDS_DIR / name)
                if name.endswith(".py")
                if not name.startswith("_")
                if not name.startswith("base.py")
        ]
    
    def command_exists(self) -> bool:
        if self.command not in self._get_all_commands():
            return False
        return True
    

    def _get_command(self) -> BaseCommand:
        filename = self.command[1:] + ".py"
        path = _COMMANDS_DIR / filename
        module_name = 'telegram.commands.%s' %  self.command[1:]
        module = imp.load_source(name=module_name, pathname=path.absolute().__str__())
        return module.Command(
            argument = self.argument,
            message_id = self.message_id,
            account = self.account
        )
