import re
import logging
import requests
from django.utils import timezone
from django.utils.translation import gettext as _
from .base import BaseCommand


class Command(BaseCommand):
    def process(self) -> dict:
        if self.argument == "ajuda":
            return self._help_body_reponse()

        if not self._is_valid_data():
            return self._not_valid_body_reponse()
        
        return self._search_body_reponse()

    
    @staticmethod
    def _remove_no_digit(cnpj: str) -> str:
        text = re.sub(r"\D", repl="", string=cnpj)
        return text

    def _search(self) -> str:
        logging.debug(f"Searching for CNPJ: {self.argument}")
        cnpj_no_digit = self._remove_no_digit(self.argument)
        context = {}
        context['cnpj'] = self.argument
        try:
            context['search'] = requests.get(f"https://receitaws.com.br/v1/cnpj/{cnpj_no_digit}").json()
        except requests.JSONDecodeError:
            logging.warning("CNPJ search API limit reached")
            return _("Many CNPJ searches are being carried out, please wait a minute and try again.")
        
        last_update = context['search'].get('ultima_atualizacao', None)
        if last_update is not None:
            try:
                last_update = timezone.datetime.fromisoformat(last_update)
                context['last_update'] = last_update.strftime("%d/%m/%Y às %H:%M:%S")
            except:
                context['last_update'] = None

        return self.render_template('telegram/commands/cnpj/search.html', context)

    def _is_valid_data(self):
        pattern=r"^[0-9]{2}\.[0-9]{3}\.[0-9]{3}/[0-9]{4}\-[0-9]{2}$"
        match_obj = re.fullmatch(pattern=pattern, string=self.argument)

        if match_obj is None:
            return False
            
        return True
    
    def _help_body_reponse(self):
        message = self.render_template('telegram/commands/cnpj/help.html')
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': message,
            'reply_to_message_id': self.message_id,
        }
    
    def _not_valid_body_reponse(self):
        message = _("This is not a valid CNPJ. For more details press the button below:")
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': message,
            'reply_to_message_id': self.message_id,
        }
    
    def _search_body_reponse(self):
        search = self._search()
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': search,
            'reply_to_message_id': self.message_id,
        }
    
    