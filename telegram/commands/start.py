from telegram.models import ChatMessage
from .base import BaseCommand
from django.template.loader import render_to_string


class Command(BaseCommand):
    def process(self) -> dict:
        message = self.render_template('telegram/commands/start.html')
        return {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': message,
            'reply_to_message_id': self.message_id,
        }