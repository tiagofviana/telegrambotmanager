import logging
from django.utils.translation import gettext as _
from .base import BaseCommand

class Command(BaseCommand):

    def has_permission(self):
        if not self.account.is_admin:
            return False
        return True 


    def process(self) -> dict:
        if self.has_permission():
            logging.warning(f"Account {self.account.telegram_id} requested user {self.argument}")
            return  {
                'method': 'sendMessage',
                'chat_id': self.account.telegram_id,
                'parse_mode': 'HTML',
                'text': f"<a href='tg://user?id={self.argument}'>User</a>",
                'reply_to_message_id': self.message_id,
            }

        return  {
            'method': 'sendMessage',
            'chat_id': self.account.telegram_id,
            'parse_mode': 'HTML',
            'text': _("This command is not valid"),
            'reply_to_message_id': self.message_id,
        }

