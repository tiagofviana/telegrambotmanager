import re
from abc import abstractmethod, ABC
from django.template.loader import render_to_string

from ..models import Account


class BaseCommand(ABC):
    def __init__(self, argument: str, message_id: int, account: Account):
        self.argument = argument
        self.message_id = message_id
        self.account = account

    @abstractmethod
    def process(self) -> dict:
        pass

    @staticmethod
    def _replace_div(text) -> str:    
        text = re.sub(r"<div\s*>", repl="", string=text)
        text = re.sub(r"</div\s*>", repl="", string=text)
        return text

    @staticmethod
    def _replace_line_break(text) -> str:    
        text = re.sub(r"<br\s*>", repl="\n", string=text)
        text = re.sub(r"<br\s*/>", repl="\n", string=text)
        return text

    @staticmethod
    def _replace_tab_code(text)-> str:
        text = re.sub(r"&emsp;", repl="    ", string=text)
        return text

    def render_template(self, template_name, context = {}) -> str:
        text = render_to_string(template_name, context)
        text = self._replace_line_break(text)
        text = self._replace_div(text)
        text = self._replace_tab_code(text)
        return text