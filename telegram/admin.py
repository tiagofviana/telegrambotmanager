import json
from typing import Any
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import HtmlFormatter
from . import models

admin.site.index_template = "telegram/admin/index.html"

@admin.register(models.Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'telegram_id', 'registration_date')
    search_fields = ('telegram_id',)
    readonly_fields = (
        'telegram_id', 'registration_date', 'first_name', 'last_name',
        'username', 'is_bot', 'last_message_processed_date'
    )
    fieldsets = (        
        (_('Telegram data'), {
            'fields': ('telegram_id', ('first_name', 'last_name'), 'username', 'is_bot'),
        }),

        (_('System data'), {
            'fields': ('registration_date', 'last_message_processed_date'),
        }),

        (_('Permissions'), {
            'fields': ('is_blocked', 'is_admin'),
        }),

    )

    def has_delete_permission(self, request, obj=None) -> bool:
        return False
    
    def has_add_permission(self, request, obj=None) -> bool:
        return False

@admin.register(models.ChatMessage)
class ChatMessageAdmin(admin.ModelAdmin):
    list_display = ('update_id', 'Account_id', 'text', 'creation_date')
    ordering = ('creation_date',)
    search_fields = ('Account_id__telegram_id',)
    readonly_fields = ('Account_id', 'update_id', 'message_id', 'creation_date', 'text', 'request_body')
    fieldsets = (        
        (_('Telegram data'), {
            'fields': ('update_id', 'message_id', 'text',),
        }),

        (_('System data'), {
            'fields': ('Account_id','creation_date',),
        }),

        ('JSON', {
            'fields': ('request_body_prettified', 'response_body_prettified'),
        }),

    )

    _already_prittified = False
    def _prettify_dictionary(self, data: dict):
        data = json.dumps(data, sort_keys=True, indent=4, ensure_ascii=False)

        formatter = HtmlFormatter(style='rainbow_dash')
        format = highlight(data.__str__(), lexer=JsonLexer(), formatter=formatter)

        if self._already_prittified is False:
            format += f'''
                <style>
                    {formatter.get_style_defs()}
                    .highlight pre {{text-wrap: wrap}}
                </style>
            '''
            self._already_prittified = True

        # Safe the output
        return mark_safe(format)

    def request_body_prettified(self, instance):
        return self._prettify_dictionary(instance.request_body)
    request_body_prettified.short_description = _("request body")


    def response_body_prettified(self, instance):        
        return self._prettify_dictionary(instance.response_body)
    response_body_prettified.short_description = _("response body")


    def has_change_permission(self, request, obj=None) -> bool:
        return False
    
    def has_add_permission(self, request, obj=None) -> bool:
        return False
    

