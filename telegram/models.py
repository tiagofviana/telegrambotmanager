from __future__ import annotations
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

class Account(models.Model):
    telegram_id = models.PositiveBigIntegerField(
        _('telegram ID'), 
        unique=True, 
        null=False, 
        db_index=True,
        primary_key=True,
    )

    first_name = models.CharField(_('first name'), max_length=255, null=False)
    last_name = models.CharField(_('last name'), max_length=255, null=True)
    username = models.CharField(_('username'), max_length=255, null=True)
    registration_date = models.DateTimeField(_('registration date'), default=timezone.now)
    last_message_processed_date = models.DateTimeField(
        verbose_name=_('date of last message processed'),
        null=True,
        blank=True
    )
    is_bot = models.BooleanField(_('is a bot'), null=False, blank=False)
    is_blocked = models.BooleanField(_('is blocked'), default=False, null=False)
    is_admin = models.BooleanField(_('is admin'), default=False, null=False)
    
    def full_name(self):
        """ Return the 'first_name' and the 'lastName' with a space in between """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()
    full_name.short_description = _('full name')

    def __str__(self):
        return f"{self.first_name} ({self.telegram_id})"

    @staticmethod
    def get_or_create_and_update(
        telegram_id: int, is_bot: bool, first_name: str,
        last_name: str or None, username: str or None
    ) -> Account:  
        """
            Get or create an account. If it has been created, 
            update if theres is any change.
        """
        defaults = {
            'first_name': first_name,
            'last_name': last_name,
            'username' : username,
            'is_bot': is_bot
        }
        account, was_created = Account.objects.get_or_create(telegram_id=telegram_id, defaults=defaults)
        
        if not was_created:
            update_fields = []
            # Look for updates
            for key in defaults.keys():
                default_value = defaults[key]
                field_value = getattr(account, key)
                if default_value != field_value:
                    setattr(account, key, default_value)
                    update_fields.append(key)
            account.save(update_fields=update_fields)

        return account  

    class Meta:
        managed = True
        db_table = 'account'
        verbose_name = _('account')
        verbose_name_plural = _('accounts')


class ChatMessage(models.Model):
    Account_id = models.ForeignKey(
        Account,
        on_delete=models.RESTRICT,
        verbose_name=_('account'), 
        db_column='Account_id',
        null=False,
        db_index=True
    )

    update_id = models.PositiveBigIntegerField(
        verbose_name=_('update ID'),
        help_text=_(
            "update's unique identifier, allows you to ignore repeated updates or to restore the correct update sequence"
        ),
        primary_key=True,
        null=False,
        db_index=True,
    )

    message_id = models.PositiveIntegerField(
        verbose_name=_('message ID'),
        null=False,
        help_text=_("unique message identifier inside the chat")
    )

    text = models.CharField(
        verbose_name=_('message text'),
        max_length=255,
        unique=False,
        null=False,
    )

    request_body = models.JSONField(
        verbose_name=_("request body"),
        null=False
    )

    response_body = models.JSONField(
        verbose_name=_("response body"),
        null=False,
    )
    
    creation_date = models.DateTimeField(
        verbose_name=_('creation date'),
        null=False,
        default=timezone.now,
    )

    def __str__(self):
        return f"{self.update_id}"

    def save(self, *args, **kwargs) -> None:
        if self._state.adding:
            self.Account_id.last_message_processed_date = timezone.now()
            self.Account_id.save(update_fields=['last_message_processed_date'])
        return super().save(*args, **kwargs)

    @staticmethod
    def is_already_responded(update_id: int):
        six_days_ago = timezone.now() - timezone.timedelta(days=6)
        # Above line will delete old chat messages
        ChatMessage.objects.filter(creation_date__lte=six_days_ago).delete()
        exists = ChatMessage.objects.filter(update_id=update_id).exists()
        return exists

    def create_response_body(self, response_body: dict ) -> dict:
        self.response_body = response_body
        self.save(update_fields=['response_body'])
        return self.response_body
    
    class Meta:
        managed = True
        db_table = 'chat_message'
        verbose_name = _('chat message')
        verbose_name_plural = _('chat messages')