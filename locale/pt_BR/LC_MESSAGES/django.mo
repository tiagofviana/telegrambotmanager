��    ?        Y         p     q       '   �     �     �     �     �  
   �  M   �     0     D     b     v     �     �     �     �     �  
                   #     /     =  #   W     {  !   �     �  B   �  @     )   T  #   ~     �     �  C   �     
	     	     	     (	     6	     D	  
   c	  	   n	     x	  
   �	  	   �	     �	  
   �	  	   �	  
   �	     �	     �	     �	     �	     	
     
  )   #
  	   M
  k   W
     �
     �
     �
  A  �
          +  )   ;     e     k     r     z     �  V   �     �          #     7     I     `     z     �     �  
   �     �     �     �     �  %        4     N     h     �  K   �  J   �  +   9  +   e     �     �  O   �                    ,     >  #   \     �     �  	   �     �     �     �     �  	   �     �     �          %     6     L     ^  +   j     �  v   �     !     *  	   3            (   '   +          <   -          .              &   >           =                       9   ?   3       8       7         	                         :   $   "       )      *           ,       #             6      
      4                                           2   5   0   ;              %       1      /   !       COMMAND LIST: Delete webhook Email and password incorrect, try again Group Groups Home Identification Login data Many CNPJ searches are being carried out, please wait a minute and try again. Missing "chat" data Missing "edited_message" data Missing "from" data Missing "id" data Missing "message" data Missing "message_id" data Missing "my_chat_member" data Missing "text" data Missing "{key}" data Permission Permissions Set webhook System data Telegram data Telegram webhook settings There isn't a "callback_query" data There isn't an update_id This account has been deactivated This command is not valid This is not a valid CNPJ. For more details press the button below: This is not a valid IP. For more details press the button below: This user does not have access permission We don't process commands from bots Webhook was deleted Webhook was set Your account was blocked, talk to the support team to find out more account accounts chat message chat messages creation date date of last message processed first name full name is a bot is a staff is active is admin is blocked last name message ID message text permissions, groups and users registration date request body response body telegram ID unique message identifier inside the chat update ID update's unique identifier, allows you to ignore repeated updates or to restore the correct update sequence user username users Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 LISTA DE COMANDOS Deletar webhook Email e senha incorretos, tente novamente Grupo Grupos Início Identificação Dados de login Muitas pesquisas de CNPJ estão sendo realizadas, aguarde um minuto e tente novamente. Falta o dado "chat" Falta o dado "edidted_message" Flata o dado "from" Falta o dado "id" FAlta o dado "message" Falta o dado "message_id" Falta o dado "my_chat_member" Falta o dado "text" Falta o dado "{key}" Permissão Permissões Configurar webhook Dados do sistema Dados do Telegram Configuração do webhook do Telegram Não contém um update_id Não contém um update_id Esta conta foi desativada Esse não é um comando válido Este não é um CNPJ válido. Para mais detalhes pressione o botão abaixo: Este não é um IP vpalido. Para mais detalhes pressione o botçao abaixo: Esse usuário não tem permissão de acesso Não processamos comandos enviados por bots Webhook foi removido Webhook foi configurado Sua conta foi bloqueada, fale com a equipe de suporte para descobrir o ocorrido conta contas Mensagem do chat mensagens do chat Data de criação do registro data da última mensagem processada nome nome completo é um bot é um funcionário Está ativado é um admin está bloqueado sobrenome ID da mensagem texto da mensagem permissões, grupos e usuários Data de cadastro corpo da requisição corpo da resposta telegram ID identificação da menssagem dentro do chat ID de atualização ID de atualização único, permite ignorar atualizações repedidas ou restaurara sequência de atualização correta usuário username usuários 